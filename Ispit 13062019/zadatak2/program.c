/* Dana je deklaracija strukture vozac u koju �e se spremiti ovakvi ulazni podaci:
* ime, prezime, naziv_ekipe, godiste, godine_iskustva, broj_odvozenih_utrka te pokazivac na strukturu tipa utrka.
* Dana je i deklaracija strukture utrka sadrzi sljedece podatke: naziv_utrke, ostvarena_pozicija.
* Potrebno je implementirati 5 funkcija: input(), avg(), search(), output() i freedom(). 
* Detalji o samim funkcijama opisani su u datoteci functions.c

* Ovaj zadatak nosi 10 bodova

* Ulaz u program je prvo broj voza�a za koje �e se podaci unositi, a nakon toga redom podaci o njima i utrkama.
* Primjer ulaznih podataka:
* 2				// broj vozaca
* Ralf          // ime prvog vozaca
* Schumacher    // prezime prvog vozaca
* Sauber        // ekipa prvog vozaca
* 1978			// godiste prvog vozaca
* 3				// godine iskustva prvog vozaca
* 2				// broj odvozenih utrka prvog vozaca
* VN_Monaca     // naziv prve utrke prvog vozaca
* 4             // ostvarena pozicija prve utrke prvog vozaca
* VN_Belgije    // naziv druge utrke prvog vozaca
* 7             // ostvarena pozicija druge utrke prvog vozaca
* Felipe        // ime drugog vozaca
* Massa         // ...
* Ferrari
* 1982
* 1
* 1
* VN_Italije
* 1

* O�ekivani izlaz:
* REZULTATI:
* Jarno Trulli, Minardi, 1974, 3, 11, 17.4
*
* NAPOMENA: Radi pojednostavljivanja unosa stringova pretpostavite nijedan string nece sadrzavati vise od jedne rijeci
*			Tako da za unos stringova koristite scanf("%s", ...);

* ovo je main datoteka koja sadr�i pozive svih potrebnih funkcija, nju ne trebate dirati

* Svi zadaci biti ce pregledani i ocijenjeni bez obzira na rezultat VPL-a.
*/

#include <stdio.h>
#include "header.h"


int  main() {
    //ovu liniju ostavite na vrhu maina. Ona se brine da vam se ne izgubi dio Outputa
	//ukoliko dodje do segmentation faulta.
	setvbuf(stdout, NULL, _IONBF, 0);
	
	int n, ind;
	VOZAC *p;
	printf("Koliko vozaca zelite unijeti:");
	scanf("%d", &n);                            // unos broja vozaca
	p = input(n);
	// unos podataka o vozacima i utrkama
	ind = search(p, n);                         // trazenje vozaca s najboljim prosjecno ostvarenim mjestom
	printf("\nREZULTATI:\n");
	output(p, ind);                             // ispis podataka o vozacu
	freedom(p, n);
	
	return 0;
}


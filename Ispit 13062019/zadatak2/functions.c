// ovdje pi�ete implementaciju svih funkcija deklariranih u headeru

#include <stdio.h>
#include <stdlib.h>
#include "header.h"

// Napisati funkciju input() koja prima jedan cijeli broj n koji ozna�ava broj vozaca.
// Funkcija treba dinami�ki alocirati polje za n vozaca te unijeti podatke u alocirano polje.
// Funkcija tako�er treba, ovisno o broju odvozenih utrka, dinami�ki alocirati polje struktura
// utrka te pokaziva� unutar strukture vozac usmjeriti na to polje te u to polje unijeti podatke
// o odvozenim utrkama. Funkcija vra�a adresu dinami�ki alociranog polja vozaca.
// ova funkcija nosi 3 boda

VOZAC* input (int n) {
    	VOZAC *X;
	X=(VOZAC*)calloc(n,sizeof(VOZAC));
	int i;
	for(i=0;i<n;i++){
		scanf("%s",X[i].ime);
		printf("LOL 1  %s  LOL2\n", X[i].ime);
		scanf("%s",X[i].prezime);
		printf("LOL 1  %s  LOL2\n", X[i].prezime);
		//fgets(X[i].ekipa,19,stdin);
		scanf("%s",X[i].ekipa);
		printf("LOL 1  %s  LOL2\n", X[i].ekipa);
		scanf("%d", &X[i].god_rodj);
		scanf("%d", &X[i].god_iskustva);
		scanf("%d", &X[i].broj_utrka);
		X[i].p=(UTRKA*)calloc(X[i].broj_utrka,sizeof(UTRKA));
		int j;
		for(j=0;j<X[i].broj_utrka;j++){
			//fgets(X[i].p[j].naziv,39,stdin);
			scanf("%s", X[i].p[j].naziv);
			printf("LOL 1  %s  LOL2\n", X[i].p[j].naziv);
			scanf("%d",&X[i].p[j].pozicija);
			//printf("%d")
		}
	}
	
	
	
	
	return X;
    
    
    
}



// Napisati funkciju avg() koja �e primiti pokaziva� na u�itano polje vozaca
// te jedan cijeli broj koji ozna�ava indeks vozaca �iju prosje�nu poziciju �elimo izra�unati.
// Funkcija treba izra�unati prosje�nu poziciju svih utrka za voza�a �iji indeks je predan funkciji.
// Funkcija vra�a izra�unatu prosje�nu poziciju.
// ova funkcija nosi 2 boda

float avg (VOZAC *p, int ind) {
    int A=p[ind].broj_utrka;
	int i;
	int suma=0;
	for(i=0;i<A;i++){
		suma+=p[ind].p[i].pozicija;
	}
	return(1.0*suma/A);

}


// Napisati funkciju search() koja �e primiti pokaziva� na u�itano polje voza�a te n koji predstavlja
// broj voza�a. Funkcija treba sekvencijalno pro�i kroz polje i prona�i voza�a s najve�om prosje�nom
// pozicijom (najdalje u poretku). Funkcija vra�a indeks tog voza�a u polju struktura.
// ova funkcija nosi 2 boda

int search (VOZAC *p, int n) {
    	float best=0;
	int W=0;
	int i;
	for(i=0;i<n;i++){
		if(avg(p,i)>best){
			best=avg(p,i);
			W=i;
		}
	}
	return W;
    
    
    
    
}


// Napisati funkciju output() koja prima pokaziva� na polje voza�a te jedan cijeli broj koji
// ozna�ava indeks voza�a �ije podatke �elimo ispisati.
// Podatke ispisati u sljede�em obliku:
// ime prezime, naziv_ekipe, godiste, godine_iskustva, broj_utrka, prosjecna_pozicija_na_dvije_decimale
// ova funkcija nosi 1 bod

void output(VOZAC *p, int ind) {
    //printf("lol%f", avg(p,2));
    	printf("%s %s, %s, %d, %d, %d, %.2f\n", p[ind].ime, p[ind].prezime, p[ind].ekipa, p[ind].god_rodj, p[ind].god_iskustva, p[ind].broj_utrka, avg(p,ind));
	return;
    
    
    
}


// Napisati funkciju freedom() koja prima pokaziva� na polje voza�a te n koji predstavlja
// broj voza�a. Funkcija treba osloboditi svu dinami�ki zauzetu memoriju.
// ova funkcija nosi 2 boda

void freedom(VOZAC *p, int n) {
    
    int i;
	for(i=0;i<n;i++){
		free(p[i].p);
	}
    
    
    
}

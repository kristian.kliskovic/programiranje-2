typedef struct utrka {
	char naziv[40];
	int pozicija;
} UTRKA;

typedef struct vozac {
	char ime[20];
	char prezime[20];
	char ekipa[20];
	int god_rodj;
	int god_iskustva;
	int broj_utrka;
	struct utrka *p;
} VOZAC;

VOZAC* input (int);
float avg (VOZAC*, int);
int search (VOZAC*, int);
void output(VOZAC*, int);
void freedom(VOZAC*, int);

#include <stdio.h>
#include <stdlib.h>
#include "header.h"

MATRICA* Zauzimanje(char* ime) {
    MATRICA *M;
    M=(MATRICA*)calloc(1,sizeof(MATRICA));
    FILE *f;
    f=fopen(ime,"r");
    fscanf(f,"%d",&M->red);
    M->podaci=(int**)calloc(M->red,sizeof(int*));
    fscanf(f,"%d",&M->stupac);
    int i;
    for(i=0;i<M->red;i++){
        M->podaci[i]=(int*)calloc(M->stupac, sizeof(int));
    }
    fclose(f);
    return M;
}

void Popunjavanje(MATRICA* M, char* ime) {
    FILE *f;
    f=fopen(ime,"r");
    int i,j;
    //printf("dimenzije: %d %d\\n", M->red, M->stupac);
    for(i=0;i<M->red;i++){
        for(j=0;j<M->stupac;j++){
            fscanf(f,"%d", &M->podaci[i][j]);
            printf("%d ", M->podaci[i][j]);
        }
        //printf("\n");
    }
    fclose(f);
}

void NovaMatrica(MATRICA* M) {
    int R=0,S=0,big=0;
    int i,j;
    for(i=0;i<M->red;i++){
        for(j=0;j<M->stupac;j++){
            if(M->podaci[i][j]>big){
                big=M->podaci[i][j];
                R=i;
                S=j;
            }
        }
    }
    for(i=0;i<M->stupac;i++){
        if(i!=S) M->podaci[R][i]=1;
    }
    for(i=0;i<M->red;i++){
        if(i!=R) M->podaci[i][S]=-1;
    }
}

void Ispisivanje(MATRICA* M) {
	int i,j;
    for(i=0;i<M->red;i++){
        for(j=0;j<M->stupac;j++){
            printf("%d ",M->podaci[i][j]);
        }
        printf("\n");
    }
}

void Oslobadjanje(MATRICA* M) {
	int i;
    for(i=0;i<M->red;i++){
        free(M->podaci[i]);
    }
    free(M->podaci);
}

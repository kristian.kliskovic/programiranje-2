#include "functions.h"

/* Zadani su vam fileovi functions.c i functions.h
 * Ovaj file ne morate mijenjati.
 * Prema pozivima funkcija u ovom fileu trebate implementirati funkcije
 * u functions.c fileu, cije su definicije u functions.h.
 * Komentari iznad funkcija vam govore sto se ocekuje od tih funkcija.
 * Kao i u proslom roku zadane su vam funkcije allocateMatrix i
 * printMatrix, a vi trebate implementirate preostale dvije funkcije.
 *
 * Tamo gdje se trazi alokacija nove matrice trebate koristiti funkciju
 * allocateMatrix, pa pogledajte kako je ta funkcija implementirana.
 * */

int main(void)
{
  setvbuf(stdout, NULL, _IONBF, 0);
  Mat *A, *Res;
  char filename[100];

  puts("input matrix filename: ");
  fgets(filename, 100, stdin);
  //ubij \n iz fgets-a
  filename[strlen(filename)-1] = '\0';
  A = newMatrixFromFile(filename);
  printMatrix(A);

  Res = sortByColumns(A);
  puts("Sorted by rows matrix:");
  printMatrix(Res);

  return 0;
}


/*
program.c functions.c
*/
#include "functions.h"

/* novina u odnosu na dosadasnje primjere je parametar init_value koji
 * cijelu matricu postavlja na tu predanu vrijednost, tj. inicjilizira
 * cijelu matricu na tu vrijednost */
Mat *allocateMatrix(int rows, int cols, int init_value) {
  Mat *M = (Mat*) malloc(sizeof(Mat));
  M->rows = rows;
  M->cols = cols;
  int i,j;
  M->data = (int**) malloc(sizeof(int*) * rows);
  for (i = 0; i < rows; i++) {
    M->data[i] = (int*) malloc(sizeof(int)*cols);
  }
  for (i = 0; i < rows; i++) {
    for (j = 0; j < cols; j++) {
      M->data[i][j] = init_value;
    }
  }
  return M;
}

void printMatrix(Mat *M) {
  int rows, cols;
  rows = M->rows;
  cols = M->cols;
  int i,j;
  for (i = 0; i < rows; i++) {
    for (j = 0; j < cols; j++) {
      printf("%d\t", M->data[i][j]);
    }
    printf("\n");
  }
}

/* Funkcija prima filename i ucitava maticu iz filea.
 * Treba alocirati novu matricu koriste�i allocateMatrix funkciju.
 * Nije bitno koju cete vrijednost odabrati za init_value, ionako sve
 * vrijednosti morate ucitati iz filea.
 * Podaci u matrici su tipa int, pa pazite pri u�itavanju.
 * Prva dva broja u fileu predstavljaju dimenzije. Zanemarite ako ima vise
 * brojeva u fileu nego sto ce se ucitati. Vi jednostavno prekinete ucitavanje
 * kada ste ucitalo onoliko brojeva kolika je matrica. 
 * Ova funkcija nosi 30% bodova zadatka
 */
Mat* newMatrixFromFile(char *filename) {
    Mat *matrica=calloc(1,sizeof(Mat));
    FILE *f=fopen(filename, "r");
    fscanf(f,"%d", &matrica->rows);
    fscanf(f,"%d", &matrica->cols);
    matrica->data=(int**)calloc(matrica->rows, sizeof(int*));
    int i,j;
    for(i=0;i<matrica->rows;i++){
        matrica->data[i]=(int*)calloc(matrica->cols, sizeof(int));
    }
    for(i=0;i<matrica->rows;i++){
        for(j=0;j<matrica->cols;j++){
            fscanf(f,"%d", &matrica->data[i][j]);
        }
    }
    return matrica;
}



/* 
 * Ova funkcija vraca NOVU matricu koja sadrzi podatke iz predane
 * matrice sortirane po stupcima. Primjer: 
 * Predana matrica 6x3:
 *   1       2       3       
 *   2       1       5       
 *   8       7       6       
 *   2       1       11      
 *   7       6       5       
 *   1       2       3       
 *  Sortirana matrica istih dimenzija:
 *   1       1       3       
 *   1       1       3       
 *   2       2       5       
 *   2       2       5       
 *   7       6       6       
 *   8       7       11      
 * Napomene: 
 * - MORATE stvoriti novu matricu i vratiti ju iz funkcije. Za
 *   alokaciju nove matrice koristite vec zadanu funkciju allocateMatrix
 * - preporuka je alocirati novu matricu, kopirati vrijednosti predane
 *   matrice u nju te novu matricu sortirati prema kriteriju
 * - implementacija samog algoritma sortiranja potpuno je na vama. Moze
 *   biti pomocu druge funkcije, moze biti unutar ove funkcije. 
 */ 

Mat* sortByColumns(Mat* mat) {
    Mat *m1;
    m1=allocateMatrix(mat->rows,mat->cols,0);
    int x,y;
    for(x=0;x<mat->rows;x++){
        for(y=0;y<mat->cols;y++){
            m1->data[x][y]=mat->data[x][y];
        }
    }
    //printf("LMAO\n");
    int i, j, min;
    int t;
    for(t=0;t<m1->rows;t++){
        //printf("LMAO2\n");
        for (i = 0; i < m1->cols; i++) {
            min = i;
            for (j = i+1; j < m1->rows; j++)
                if (m1->data[j][t] < m1->data[min][t]) 
                    min = j;
            //swap(&m1->data[i][t], &m1->data[min][t]);
            int aux;
            aux = m1->data[i][t];
            m1->data[i][t] = m1->data[min][t];
            m1->data[min][t] = aux;
        }
    }
    return m1;
}


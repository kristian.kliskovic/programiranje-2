#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct matrix {
  int rows;
  int cols;
  int **data;
} Mat;

Mat* allocateMatrix(int rows, int cols, int init_value);


Mat* newMatrixFromFile(char *filename);

Mat* sortByColumns(Mat* mat);

void printMatrix(Mat *M);


#endif


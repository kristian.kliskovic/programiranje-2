typedef struct zaposlenik {
	int id;
	char *name;
	char *surname;
	char job_title[20];
	float salary;
	struct zaposlenik* prevNode;
	struct zaposlenik* nextNode;
} ZAPOSLENIK;

void inputNodeData(ZAPOSLENIK*);
int increaseSalary(ZAPOSLENIK *);
int traverseDLList(ZAPOSLENIK*);
ZAPOSLENIK* createDLList(void);
ZAPOSLENIK* insertNewNodeDLList(ZAPOSLENIK*);
ZAPOSLENIK* deleteWholeDLList(ZAPOSLENIK*);

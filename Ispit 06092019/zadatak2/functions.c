// ovdje pi�ete implementaciju svih funkcija deklariranih u headeru

#include <stdio.h>
#include <stdlib.h>
#include "header.h"

// ovdje pi�ete implementaciju funkcije inputNodeData(). Funkcija prima pokazivac na
// cvor za koji se podaci unose. Funkcija treba unijeti sve potrebne podatke s tipkovnice 
// i spremiti ih u predani cvor. Obratite pozornost da memorija za ime, prezime i radno mjesto
// nije alocirana. Funkcija ne vraca nikakvu vrijednost.
// ova funkcija nosi 3 boda

void inputNodeData(ZAPOSLENIK* node) {

    scanf("%d", &(node->id));
    
    node->name=(char*)malloc(20*sizeof(char));
    scanf("%s",node->name);
    
    node->surname=(char*)malloc(20*sizeof(char));
    scanf("%s",node->surname);
    
    //node->job_title=(char*)malloc(20*sizeof(char));
    scanf("%s",node->job_title);
    
    scanf("%f",&(node->salary));
    
    //samo ako trebam popuniti podatke strukture koju cu dodati na 1. mjesto
    node->nextNode=NULL;
    node->prevNode=NULL;
    

}

// ovdje pi�ete implementaciju funkcije increaseSalary(). Funkcija prima pokazivac na headNode.
// Funkcija treba proci kroz cijeli povezani popis i svim zaposlenicima koji imaju placu
// manju od prosjecne, povisiti placu za 20%. Funkcija vraca broj zaposlenika kojima
// je placa povecana.
// ova funkcija nosi 4 boda

int increaseSalary(ZAPOSLENIK *headNode) {
    
	int brojac=0;
	int broj_zaposlenika=0;
	float suma_placa=0;
	float prosjecna_placa;
	
	ZAPOSLENIK* headNode1=headNode;
	//prvo ra�unamp prosjecnu placu svih zaposlenika
	if(headNode1== NULL) {
	    return -1;
	    
	}else{
	    while(headNode1) {
	        broj_zaposlenika++;
	        suma_placa+=headNode1->salary;
	        headNode1=headNode1->nextNode;
	    }
	}
	
	prosjecna_placa=suma_placa/broj_zaposlenika;
	
	//sada gledamo koliko zaposlenika ima placu ispod prosjecne
	while(headNode){
	    if(headNode->salary<prosjecna_placa){
	        brojac++;
	        headNode->salary=headNode->salary + 0.20*headNode->salary;
	    }
	    headNode=headNode->nextNode;
	}
	
	return brojac;
}

// ovdje pisete implementaciju funkcije insertNewNode(). Funkcija prima pokazivac na headNode.
// FUnkcija treba dinamicki zauzeti memoriju za novi cvor, unijeti podatke u njega i smjestiti 
// ga na pocetak povezanog popisa. Funkcija vraca adresu novog cvora koji sada postaje novi headNode.
// ova funkcija nosi 3 boda

ZAPOSLENIK* insertNewNodeDLList(ZAPOSLENIK* headNode) {
    
    ZAPOSLENIK* newHeadNode = (ZAPOSLENIK*)calloc(1, sizeof(ZAPOSLENIK));
    
    if(newHeadNode == NULL) {
        perror("Kreiranje");
        return headNode;
    }else{
        inputNodeData(newHeadNode);
        newHeadNode->nextNode = headNode;
        newHeadNode->prevNode = NULL;
        headNode->prevNode = newHeadNode;
    }
    return newHeadNode;

}

// kreiranje povezanog popisa i umetanje prvog �lana
ZAPOSLENIK* createDLList(void) {

	ZAPOSLENIK *headNode = (ZAPOSLENIK*)calloc(1, sizeof(ZAPOSLENIK));

	if (headNode == NULL) {

		perror("Kreiranje");
		return NULL;
	}
	else {

		inputNodeData(headNode);
		headNode->prevNode = NULL;
		headNode->nextNode = NULL;
	}
	return headNode;
}

// obilazak povezanog popisa i ispis svih clanova
int traverseDLList(ZAPOSLENIK* traverseNode) {

	int counter = 0;

	if (traverseNode == NULL) {

		return -1;
	}
	else {

		while (traverseNode) {
			counter++;
			printf("ID: %d - %s %s, %s, iznos place: %.2f\n", traverseNode->id, traverseNode->name, traverseNode->surname, traverseNode->job_title, traverseNode->salary);
			traverseNode = traverseNode->nextNode;
		}
	}
	return counter;
}

// oslobadjanje memorije za cijeli povezani popis
ZAPOSLENIK* deleteWholeDLList(ZAPOSLENIK* traverseNode) {

	while (traverseNode->nextNode != NULL) {
		traverseNode = traverseNode->nextNode;
		free(traverseNode->prevNode->name);
		free(traverseNode->prevNode->surname);
		//free(traverseNode->prevNode->job_title);
		free(traverseNode->prevNode);
	}
	return NULL;
}

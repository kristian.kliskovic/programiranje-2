// Napisati C program u kojemu se kreira dvostruko povezani popis. Kreirana je struktura koja opisuje
// zaposlenika (id, ime, prezime, radno mjesto, placa te pokazivaci na prethodni i sljedeci cvor).
// Dane su funkcije createDLList() i traverseDLList(). Potrebno je dodatno implementirati 4 funkcije
// insertNewNodeDLList(), inputNodeData(), increaseSalary() i deleteWholeDLList().
// Detalji o samim funkcijama opisani su u datoteci functions.c

// Ovaj zadatak nosi 10 bodova

// primjer unosa:
// 1            // id prvog zaposlenika
// Pero         // ime prvog zaposlenika
// Peric        // prezime prvog zaposlenik
// direktor     // radno mjesto prvog zaposlenika
// 12000        // iznos place prvog zaposlenika
// 2            // id drugog zaposlenika
// Marko        // ...
// Markic
// limar
// 3400
// 3
// Davor
// Davic
// tokar
// 2800

// ocekivani izlaz:
// REZULTATI:
// ID: 3 - Davor Davic, tokar, iznos place: 2800.00
// ID: 2 - Marko Markic, limar, iznos place: 3400.00
// ID: 1 - Pero Peric, direktor, iznos place: 12000.00
// Ispis nakon povisice:
// ID: 3 - Davor Davic, tokar, iznos place: 3360.00
// ID: 2 - Marko Markic, limar, iznos place: 4080.00
// ID: 1 - Pero Peric, direktor, iznos place: 12000.00
// Placa je povecana za 2 zaposlenika!

// ovo je main datoteka koja sadr�i pozive svih potrebnih funkcija, nju ne trebate dirati

// Svi zadaci biti ce pregledani i ocijenjeni bez obzira na rezultat VPL-a.

#include <stdio.h>
#include "header.h"


int  main() {
    //ovu liniju ostavite na vrhu maina. Ona se brine da vam se ne izgubi dio Outputa
	//ukoliko dodje do segmentation faulta.
	setvbuf(stdout, NULL, _IONBF, 0);
	
	int n, i, cnt;
	
	ZAPOSLENIK *headNode = NULL;
	
	printf("Koliko zaposlenika zelis unijeti? ");
	scanf("%d", &n);

	headNode = createDLList();                              // kreiranje povezanog popisa i umetanje prvog �lana

	for (i = 0; i < n-1; i++)
	{
		headNode = insertNewNodeDLList(headNode);           // umetanje sljede�ih n-1 �lanova
	}
	
	printf("REZULTATI:\n");
	
	traverseDLList(headNode);                               // obilazak povezanog popisa i ispis svih �lanova

	cnt = increaseSalary(headNode);                         // povecanje place svim zaposlenicima koji imaju placu manju od prosjeka
	                                                        // funkcija vraca broj zaposlenika kojima je placa povecana
	printf("Ispis nakon povisice:\n");
	traverseDLList(headNode);                               // ponovni obilazak povezanog popisa i ispis svih �lanova nakpn povisice
	printf("Placa je povecana za %d zaposlenika!\n", cnt);  // broj zaposlenika kojima je placa povecana

	deleteWholeDLList(headNode);                 // osloba�anje memorije za cijeli povezani popis    

	return 0;
}

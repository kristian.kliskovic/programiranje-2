#ifndef HEADER_H
#define HEADER_H

typedef struct spremnik {
	int red;
	int stupac;
	int** podaci;
}MATRICA;

void Zauzimanje(MATRICA*, char*);
void Popunjavanje(MATRICA*, char*);
void NovaMatrica(MATRICA*);
void Ispisivanje(MATRICA*);
void Oslobadjanje(MATRICA*);

#endif


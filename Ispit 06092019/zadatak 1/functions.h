#include <stdio.h>
#include <stdlib.h>
#include "header.h"

int prost(int x){
    int i;
    if(x==1) return 0;
    if(x==0) return 0;
    if(x==2) return 1;
    for(i=2;i<x/2+1;i++){
        if(x%i==0) return 0;
    }
    return 1;
}
// ova funkcija nosi 2 boda
void Zauzimanje(MATRICA *M, char* ime) {
    FILE *f;
    printf("lol1\n");
    f=fopen(ime,"r");
    fscanf(f,"%d",&M->red);
    printf("lol2 %d\n", M->red);
    M->podaci=(int**)calloc(M->red,sizeof(int*));
    fscanf(f,"%d",&M->stupac);
    printf("lol3 %d\n", M->stupac);
    int i;
    for(i=0;i<M->red;i++){
        M->podaci[i]=(int*)calloc(M->stupac, sizeof(int));
    }
    fclose(f);
}

// ova funkcija nosi 2 boda
void Popunjavanje(MATRICA* M, char* ime) {
    FILE *f;
    f=fopen(ime,"r");
    int i,j;
    printf("dimenzije: %d %d\n", M->red, M->stupac);
    for(i=0;i<M->red;i++){
        for(j=0;j<M->stupac;j++){
            fscanf(f,"%d", &M->podaci[i][j]);
            printf("%d ", M->podaci[i][j]);
        }
        printf("\n");
    }
    fclose(f);
}

// ova funkcija nosi 3 boda
void NovaMatrica(MATRICA* M) {
    int i,j;
    int suma=0;
    for(j=0;j<M->stupac;j++){
        suma=0;
        for(i=0;i<M->red;i++){
            if(prost(M->podaci[i][j])==1) suma+=M->podaci[i][j];
        }
        printf("SUMA=%d\n", suma);
        for(i=0;i<M->red;i++){
            M->podaci[i][j]-=suma;    
        }
    }
}

// ova funkcija nosi 1 bod
void Ispisivanje(MATRICA* M) {
    int i,j;
    for(i=0;i<M->red;i++){
        for(j=0;j<M->stupac;j++){
            printf("%d ",M->podaci[i][j]);
        }
        printf("\n");
    }
}

// ova funkcija nosi 2 boda
void Oslobadjanje(MATRICA* M) {
    int i;
    for(i=0;i<M->red;i++){
        free(M->podaci[i]);
    }
    free(M->podaci);
    free(M);
}








/* 
Napisati program koji �e pomo�u funkcija pozvanih iz glavnog dijela programa izvr�iti odre�ene operacije nad �lanovima strukture.

Zadana je struktura struct spremnik i pomo�u klju�ne rije�i typedef kreiran je novi tip podatka nazvan MATRICA. 
Struktura sadr�i tri �lana koji opisuju matricu, dvije cjelobrojne vrijednosti koje predstavljaju red i stupac, 
te dvostruki pokaziva� na cjelobrojni tip podatka koji se treba usmjeriti na dinami�ki zauzeti memorijski prostor i koji �e predstavljati sadr�aj matrice.

Pomo�u funkcije Zauzimanje() potrebno je otvoriti datoteku dimenzija.txt unutar koje se nalazi broj redova i stupaca, 
te u�itati dimenzije matrice u �lanove strukture predane funkciji. Nakon toga, potrebno je dinami�ki zauzeti memoriju za tre�i �lan 
strukture koji predstavlja sadr�aj matrice. Funkcija Zauzimanje() ne vra�a ni�ta, a za parametar prima pokaziva� na strukturu MATRICA
te naziv datoteke koju treba otvoriti.

Pomo�u funkcije Popunjavanje() potrebno je u�itati sadr�aj iz datoteke podaci.txt u �lan strukture podaci. 
Funkcija Popunjavanje() ne vra�a vrijednost, a kao parametre prima pokaziva� na strukturu i naziv datoteke koju treba otvoriti.

Pomo�u funkcije NovaMatrica() potrebno je prona�i sumu prostih brojeve pojedinog stupca. Nakon toga svaki element stupca umanjiti za dobivenu sumu prostih brojeva tog stupca.
Za primjer pogledati primjer ispisa i rezultat. 
Funkcija NovaMatrica() ne vra�a vrijednost, a kao parametar prima pokaziva� na strukturu.

Pomo�u funkcije Ispisivanje() ispisati sadr�aj novonastale matrice u matri�nom obliku, gdje su elementi razdvojeni jednim razmakom, 
a redovi znakom za novi red. Funkcija Ispis() ne vra�a vrijednost, a kao parametar prima pokaziva� na strukturu.

Funkcija Osloba�anje() slu�i za osloba�anje memorije sadr�aja matrice. 
Funkcija Osloba�anje() ne vra�a vrijednost, a kao parametar prima pokaziva� na strukturu.

Primjer ispisa (primjer za matricu 3x3):
3    11    5
7    2     8
4    0     7

REZULTATI:
-7    -2    -7
-3    -11   -4
-6    -13   -11
Svi zadaci biti �e pregledani i ocijenjeni bez obzira na rezultat VPL-a.

// Ovaj zadatak nosi 10 bodova
*/

// ovo je main datoteka koja sadr�i pozive svih potrebnih funkcija, nju ne trebate dirati
 
#include <stdio.h>
#include "header.h"

int main(void) {
	//ovu liniju ostavite na vrhu maina. Ona se brine da vam se ne izgubi dio Outputa
	//ukoliko dodje do segmentation faulta.
	//setvbuf(stdout, NULL, _IONBF, 0);
    printf("LOL \n");
	MATRICA M;
	
	Zauzimanje(&M, "dimenzija.txt");
	Popunjavanje(&M, "podaci.txt");
	NovaMatrica(&M);
	printf("REZULTATI:\n");
	Ispisivanje(&M);
	Oslobadjanje(&M);
	
	return 0;
}

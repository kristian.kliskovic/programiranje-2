//nisam isao algoritmom binarne pretrage
//ostale je vjv okej
//nije mi se dalo provjeravat
//provjera se "ostavlja kao vjezba čitaocu"
//sretno

#include<stdio.h>
#include<stdlib.h>
#include"header.h"

int brojElemenata(char *ime){
	FILE *f=fopen(ime,"r");
	int n;
	fscanf(f,"%d", &n);
	fclose(f);
	return n;
}

FILM* ucitavanjeFilmova(char *ime, int n){
	FILM *polje=(FILM*)calloc(n, sizeof(FILM));
	FILE *f=fopen(ime,"r");
	int i;
	for(i=0;i<n;i++){
		fscanf(f, "%d", polje[n].rBroj);
		fscanf(f, "%s", polje[n].naslov);
		fscanf(f, "%s", polje[n].glumac);
		fscanf(f, "%s", polje[n].vrsta);
	}
	fclose(f);
}


void swap (FILM *x, FILM *y) {
    FILM aux;
    aux = *x;
    *x = *y;
    *y = aux;
}

void bubbleSort (FILM polje[], int n) {
    int i, j;
    for (i = 0; i < n-1; i++)
	    for (j = 0; j < n-1-i; j++)
		    if (polje[j+1].vrsta < polje[j].vrsta)
                swap(&polje[j], &polje[j+1]);
}

void sortiranjeFilmova(FILM *poljeF, int n){
	bubbleSort(poljeF, n);
}

void brisanjeFilmaIzDatoteke(int broj, char *ime){
	int n=brojElemenata("ime");
	FILM *novo_polje;
	
	if(broj!=-1) n--;
	novo_polje=(FILM*)calloc(n,sizeof(FILM));
	
	FILE *f=fopen(ime,"rb");
	int i;
	for(i=0;i<n;i++){
		fscanf(f, "%s", novo_polje[n].naslov);
		fscanf(f, "%s", novo_polje[n].glumac);
		fscanf(f, "%s", novo_polje[n].vrsta);
		fscanf(f, "%d", novo_polje[n].rBroj);
		
		if(novo_polje[n].rBroj==broj){
			i--; //da prepise preko njega 
		}
	}
	fclose(f);
	
	f=fopen("new_file.bin","wb+");
	fprintf(f, "%d",n);
	for(i=0;i<n;i++){
		fprintf(f,"%s", novo_polje[i].naslov);
		fprintf(f,"%s", novo_polje[i].glumac);
		fprintf(f,"%s", novo_polje[i].vrsta);
		fprintf(f,"%d", novo_polje[i].rBroj);
	}
	fclose(f);
	remove("filmovi.bin");
	rename("new_file.bin", "filmovi.bin");


}

int pretragaFilmova(FILM *polje, int n, int trazeni){
	int i;
	for(i=0;i<n;i++){
		if(polje[i].rBroj==trazeni) return i;
	}
	printf("Nema takvog filma");
	return -1;
}
FILM* oslobadanje(FILM *polje, int n){
	free(polje);
	return NULL;//NULL
}





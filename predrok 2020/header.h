#ifndef HEADER_H
#define HEADER_H
typedef struct videoteka  {
	char naslov[50];
	char glumac[50];
	char vrsta[40];
	long int rBroj;
}FILM;

int brojElemenata(char *ime);
FILM* ucitavanjeFilmova(char *ime, int n);
void sortiranjeFilmova(FILM *poljeF, int n);
void brisanjeFilmaIzDatoteke(int broj, char *ime);
FILM* oslobadanje(FILM *polje, int n);
#endif
